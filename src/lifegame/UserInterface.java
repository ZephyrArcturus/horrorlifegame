package lifegame;

public class UserInterface{

    public static void displayGameBoard(Board board){
        System.out.print("\033[H\033[2J");
        System.out.flush();
        String toDisplay;
        for (int x=0; x<20; x++){
            toDisplay = "";
            for (int y=0; y<20; y++) {
                if (board.content[x][y]){
                    toDisplay += "#";
                } else {
                    toDisplay += "_";
                }
            }
            System.out.println(toDisplay);
        }
    }

    public static void displayEndMessage() {
        System.out.println("Game over");
    }

}