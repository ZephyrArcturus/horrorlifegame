package lifegame;

public class LifeGame {


    public LifeGame(){
        Board nextBoard = new Board(20);
        Board currentBoard = new Board(20);
        currentBoard.content[17][17] = true;
        currentBoard.content[18][17] = true;
        currentBoard.content[17][18] = true;
        currentBoard.content[19][18] = true;
        currentBoard.content[17][19] = true;

        while(!currentBoard.eq(nextBoard)){
            UserInterface.displayGameBoard(currentBoard);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            nextBoard = currentBoard.cloneBoard();
            currentBoard = new Board(20);
            for (int x=0; x<20; x++){
                for (int y=0; y<20; y++) {
                    int numberOfAdjascentAliveCells = 0;
                    for (int vertical= -1; vertical <= 1; vertical++){
                        for (int horizontal = -1; horizontal <= 1; horizontal++){
                            if (!(vertical== 0 && horizontal==0)){
                                if (x+horizontal >= 0 && x+horizontal < 20 && y + vertical >= 0 && y+vertical < 20){
                                    boolean ok =  nextBoard.content[x+horizontal][y+vertical];
                                    if (ok){
                                        numberOfAdjascentAliveCells ++;
                                    }
                                }
                            }
                        }
                    }
                    if ( nextBoard.content[x][y]){
                        if (numberOfAdjascentAliveCells < 2 || numberOfAdjascentAliveCells > 3){
                             currentBoard.content[x][y] = false;
                        } else {
                             currentBoard.content[x][y] = true;
                        }
                    } else {
                        if (numberOfAdjascentAliveCells == 3){
                             currentBoard.content[x][y] = true;
                        } else {
                             currentBoard.content[x][y] = false;
                        }
                    }
                }
            }
        }
        UserInterface.displayEndMessage();
    }
}