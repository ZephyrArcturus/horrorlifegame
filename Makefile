SOURCES = $(shell find src/lifegame -type f -name '*.java')
all: build

build:
	mkdir -p bin
	javac $(SOURCES) -d bin

run: build
	java -cp bin/ lifegame.Main

